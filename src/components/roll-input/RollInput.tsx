import { useState } from "react";
import { useForm } from "react-hook-form";
import styles from './RollInput.module.scss';

interface Score {
  score: number;
}

interface Props {
  registerThrow: (roll: number) => void,
  currentFrame: number
}


const RollInput = ({ registerThrow, currentFrame }: Props) => {
  const { register, errors, handleSubmit, reset } = useForm<Score>();
  const [scoreError, setScoreError] = useState(false as boolean);
  const [scoreErrorMessage, setScoreErrorMessage] = useState("" as String);
  const [currentAttempt, setCurrentAttempt] = useState(1 as number);
  const [prevScore, setPrevScore] = useState(0 as number);



  const onSubmit = (data: Score, e: any) => {
    if (!scoreError) {

      if (currentFrame != 9 && currentAttempt % 2 == 0 && data.score + prevScore > 10) {
        setScoreError(true);
        setScoreErrorMessage(`Score should be less than ${10 - prevScore}`);
        return;
      }

      registerThrow(data.score);
      setPrevScore(data.score);
      if (data.score != 10) {
        setCurrentAttempt(currentAttempt + 1);
      } else {
        setCurrentAttempt(currentAttempt + 2);
      }
      e.target.value = 5;
      reset({});
    }
  }

  const validateScore = (num: number) => {
    if (isNaN(num)) {
      return;
    }

    setScoreError(false);
    setScoreErrorMessage('');

    if (currentFrame != 9 && currentAttempt % 2 == 0) {
      if (num + prevScore > 10) {
        setScoreError(true);
        setScoreErrorMessage(`Score should be less than ${10 - prevScore}`);
      } else {
        setScoreError(false);
      }
    } else {
      if (currentAttempt % 2 == 0 && prevScore != 10) {
        if (num + prevScore > 10) {
          setScoreError(true);
          setScoreErrorMessage(`Score should be less than ${10 - prevScore}`);
        } else {
          setScoreError(false);
        }

      }
    }
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.field}>

          <input
            type="number"
            id="score"
            name="score"
            className={styles.input}
            onChange={(e) => { validateScore(+e.target.value); return false; }}
            ref={register({
              required: "Score is required",
              valueAsNumber: true,
              min: {
                value: 0,
                message: "Score should be positive number"
              },
              max: {
                value: 10,
                message: "Score should be less than 10 "
              },
            })}
          />
        </div>
        {!scoreError && <button className="btn btn-primary" type="submit">Enter your score</button>}
        {errors && <p className={styles.error}>{errors.score?.message}</p>}
        {scoreError && <p className={styles.error}>{scoreErrorMessage}</p>}

      </form>
    </div>
  );
};

export default RollInput;
