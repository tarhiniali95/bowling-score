const Header = () => {
  return (
    <div>
      <h1> Welcome to bowlify :)</h1>
      <p>A simple app that let you calculate your bowling score</p>
    </div>
  );
};

export default Header;
