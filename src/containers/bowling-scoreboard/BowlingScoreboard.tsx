import React, { useEffect, useState } from 'react';
import range from 'lodash/range';
import BowlingFrame from '../../components/bowling-frame/BowlingFrame';
import RollInput from "../../components/roll-input/RollInput";
import { BowlingFrameScore } from '../../models/bowlingFrameScore';
import { gameHasFinished } from "../../services/gameStatusCheckerService";
import { updateScoresAfterRoll } from '../../services/scoreCalculatorService';
import styles from './BowlingScoreboard.module.scss';


interface Props {
  maxFrames: number;
}

const BowlingScoreBoard = ({ maxFrames }: Props) => {
  const [currentFrame, setCurrentFrame] = useState(0);
  const [finished, setFinished] = useState(false);
  const [scores, setScores] = useState<BowlingFrameScore[]>([]);

  useEffect(() => {
    setScores(initializeNewGame(maxFrames))
  }, [maxFrames]);


  const registerThrow = (roll: number) => {
    scores[currentFrame].throws.push(roll);
    setScores(updateScoresAfterRoll(scores, currentFrame));

    if (currentFrame !== maxFrames && (scores[currentFrame].throws.length === 2 || roll === 10)) {
      setCurrentFrame(currentFrame + 1);
    }
    if (gameHasFinished(scores[currentFrame], maxFrames)) {
      setFinished(true);
    }
  }

  const renderBowlingFrames = () => {
    return scores.map((frameScore: BowlingFrameScore) => (
      <BowlingFrame
        key={frameScore.frameNumber}
        frameScore={frameScore}
        activeFrame={currentFrame}
      />
    ))
  }

  return (
    <div>
      <div className={styles.bowlingGameWrapper}>
        {renderBowlingFrames()}
      </div>
      {finished
        ? <div>
          <h1 className={styles.finished}>Game finished</h1>
          <button className="btn btn-primary"
            onClick={() => {
              setScores(initializeNewGame(maxFrames));
              setFinished(false);
              setCurrentFrame(0);
            }
            }>Play again</button>
        </div>
        : <RollInput currentFrame={currentFrame} registerThrow={registerThrow} />
      }
    </div>
  )
}

const initializeNewGame = (maxFrames: number): BowlingFrameScore[] => {
  return range(0, maxFrames + 1).map((i) => ({
    frameNumber: i,
    throws: [],
  }));
}


export default BowlingScoreBoard;
