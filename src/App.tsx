import BowlingScoreBoard from "./containers/bowling-scoreboard/BowlingScoreboard";
import Header from './containers/header/Header';


function App() {
  return (
    <div className={'pageContainer'}>
      <Header />
      <BowlingScoreBoard maxFrames={9} />
    </div>
  );
}

export default App;
