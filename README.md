#Bowling score calculator

##	Overview
The purpose of this document is to describes Bowling score calculator. The calculator is responsible for calculating the score of a single player.

## Tech Stack Used
1. React
2. Typescript

## Scenarios
1. User to start the game

###	User to start the game

Behind the scenes:
- The user enter the score after each roll while the game hasn't finished
- The system calculates the overall score

## Todos
Things to add to the project later on:
- Connect the app to a backend in order to save the score
- Add multiple users option instead of only one user


## To run the app
npm i && npm start


